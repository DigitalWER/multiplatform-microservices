package com.marcin.msv.configuration;

public interface TokenGenerator {
    String generateToken(String username);
}
