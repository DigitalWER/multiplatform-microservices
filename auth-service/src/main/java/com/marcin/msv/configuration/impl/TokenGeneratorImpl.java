package com.marcin.msv.configuration.impl;

import com.marcin.msv.configuration.TokenGenerator;
import com.marcin.msv.model.ServiceToken;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
@Slf4j
@Service
@RequiredArgsConstructor
public class TokenGeneratorImpl implements TokenGenerator {

    public String generateToken(String username) {
        String randomizedUUID = UUID.randomUUID().toString();
        ServiceToken token = new ServiceToken(
                randomizedUUID,
                new Date(),
                new Date(System.currentTimeMillis() + 30 * 60 * 1000),
                username);
        log.debug("Token was created at {} and is valid till {}", token.getCreatedAt(), token.getExpiredAt());
        // Define the claims for the token
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", token.getEmailPart());
        claims.put("issuedAt", token.getCreatedAt());
        claims.put("expiresAt", token.getExpiredAt());

        // Sign the token using a secret key
        String secretKey = "secretKey";
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secretKey.getBytes())
                .compact();
    }
}
