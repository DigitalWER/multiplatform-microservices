package com.marcin.msv.repository;

import com.marcin.msv.model.ServiceToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenRepository extends JpaRepository<ServiceToken, Long> {
}
