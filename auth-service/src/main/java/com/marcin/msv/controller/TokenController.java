package com.marcin.msv.controller;

import com.marcin.msv.configuration.TokenGenerator;
import com.marcin.msv.repository.TokenRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RequiredArgsConstructor
@RestController
public class TokenController {

    TokenRepository tokenRepository;

    @Autowired
    private final TokenGenerator tokenGenerator;

    @PostMapping("/generate-token")
    public String getUserToken(@RequestBody String username){
        log.debug("Generating token for username: {}", username);
        return tokenGenerator.generateToken(username);
    }
}
