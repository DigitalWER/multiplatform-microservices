package com.marcin.msv.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "user_tokens")
public class ServiceToken {
    @Id
    @SequenceGenerator(
            name = "confirmation_token_sequence",
            sequenceName = "confirmation_token_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "confirmation_token_sequence"
    )
    private Long id;

    @Column(nullable = false)
    private String token;

    @Column(nullable = false)
    private Date createdAt;

    @Column(nullable = false)
    private Date expiredAt;

    private Date confirmedAt;

//    @ManyToOne
//    @JoinColumn(
//            nullable = false,
//            name = "user-service.app_user_id"
//    )
    @Column(nullable = false)
    private String emailPart;

    public ServiceToken(String token, Date createdAt, Date expiredAt, String emailPart) {
        this.token = token;
        this.createdAt = createdAt;
        this.expiredAt = expiredAt;
        this.emailPart = emailPart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceToken serviceToken = (ServiceToken) o;
        return token.equals(serviceToken.token) && createdAt.equals(serviceToken.createdAt) && expiredAt.equals(serviceToken.expiredAt) && emailPart.equals(serviceToken.emailPart);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, createdAt, expiredAt, emailPart);
    }
}
