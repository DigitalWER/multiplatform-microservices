package com.digital.msp.mspuserproviderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MspUserProviderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MspUserProviderServiceApplication.class, args);
	}

}
