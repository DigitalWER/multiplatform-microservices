package com.digital.msp.mspuserservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MspUserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MspUserServiceApplication.class, args);
	}

}
