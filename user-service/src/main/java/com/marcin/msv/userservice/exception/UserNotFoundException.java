package com.marcin.msv.userservice.exception;

import com.marcin.msv.userservice.model.user.User;

public class UserNotFoundException extends RuntimeException {
    private static final String USER_NOT_FOUND_MESSAGE = "User not found";

    public UserNotFoundException(){
        super(USER_NOT_FOUND_MESSAGE);
    }

    public UserNotFoundException(Long id){
        super(USER_NOT_FOUND_MESSAGE + " with id " + id);
    }
    public UserNotFoundException(User user){
        super(USER_NOT_FOUND_MESSAGE + "! User might not be registered or provided data are wrong " + user.toString());
    }
}
