package com.marcin.msv.userservice.model.user;

public enum Sex {
    MALE, FEMALE
}
