package com.marcin.msv.userservice.controller;

import com.marcin.msv.userservice.configuration.ApplicationVersion;
import com.marcin.msv.userservice.exception.UserNotFoundException;
import com.marcin.msv.userservice.model.user.LoggingModel;
import com.marcin.msv.userservice.model.user.Role;
import com.marcin.msv.userservice.model.user.User;
import com.marcin.msv.userservice.configuration.service.UserService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping(ApplicationVersion.VERSION_1_0 + "/user")
public class UserController {

    private UserService userService;

    @GetMapping("/get-all")
    private ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.getAllUsers();
        return ResponseEntity.ok(users);
    }

    @PostMapping("/save")
    private ResponseEntity<User> saveUser(@Valid @RequestBody User user) {
        userService.saveUser(user);
        return ResponseEntity.ok(user);
    }

    @PostMapping("/sign-up/")
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        userService.saveUser(user);
        log.info("User with mail: " + user.getEmail() + " is created!");
        String token = generateToken(user);
        Cookie cookie = setCookie(token, 30 * 60);
        URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
                .replacePath("/users/{id}")
                .buildAndExpand(user.getId())
                .toUri();
        return ResponseEntity.created(location).header("Set-Cookie", cookie.toString()).build();
    }

    @PostMapping("/log-in/")
    public ResponseEntity<String> logIn(@Valid @RequestBody LoggingModel loggingModel){
        User user = userService.areLoggingDataCorrect(loggingModel);
        if (user != null){
            String token = generateToken(user);
            Cookie cookie = setCookie(token, 30 * 60);
            return ResponseEntity.ok().header("Set-Cookie", cookie.toString()).build();
        } else {
            return ResponseEntity.status(401).build();
        }
    }

    @PostMapping("/log-off")
    public ResponseEntity<String> logOff(HttpServletResponse response) {
        Cookie cookie = setCookie(null, 0);
        response.addCookie(cookie);
        return ResponseEntity.ok("Logged off successfully");
    }

    @GetMapping("/all")
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        Optional<User> user = Optional.ofNullable(userService.getUserById(id));
        if (user.isEmpty())
            throw new UserNotFoundException(id);
        return ResponseEntity.ok(user.get());
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUserById(@PathVariable Long id) {
        userService.deleteById(id);
    }

    @DeleteMapping("/delete-all")
    public void deleteAllUsersDespiteAdmin(@PathVariable Long id) {
        userService.deleteAll();
    }

    @PostMapping("/change-role/{id}/{role}/")
    public void changeRoleForUser(@PathVariable Role role, @PathVariable Long id){
        userService.changeRoleToUser(id, role);
    }

    private String generateToken(User user){
        String token = "";
        try {
            token = userService.generateToken(user.getEmail());
            log.info("Generated token: " + token);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return token;
    }

    private Cookie setCookie(String token, Integer length) {
        Cookie cookie = new Cookie("token", token);
        cookie.setHttpOnly(true);
        cookie.setMaxAge(length); // 30 minutes
        cookie.setPath("/");
        return cookie;
    }
}
