package com.marcin.msv.userservice.model.address;

public enum BuildingType {
    SINGLE, MULTI
}
