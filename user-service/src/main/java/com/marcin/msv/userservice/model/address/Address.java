package com.marcin.msv.userservice.model.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "address_table")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String street;
    private String country;
    @JsonProperty("post_code")
    private String postCode;
    @JsonProperty("building_number")
    private String buildingNum;
    @JsonProperty("flat_number")
    private Integer flatNumber;
    @JsonProperty("building_type")
    @Column(columnDefinition = "VARCHAR(255)")
    @Enumerated(EnumType.STRING)
    private BuildingType buildingType;

    public Address(String street, String country, String postCode, String buildingNum, Integer flatNumber) {
        this.street = street;
        this.country = country;
        this.postCode = postCode;
        this.buildingNum = buildingNum;
        this.flatNumber = flatNumber;
    }

    @PrePersist
    @PreUpdate
    private void setBuildingType() {
        if (this.flatNumber != null && this.flatNumber > 0) {
            this.buildingType = BuildingType.MULTI;
        } else {
            this.buildingType = BuildingType.SINGLE;
        }
    }
}
