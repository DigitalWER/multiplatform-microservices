package com.marcin.msv.userservice.model.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class LoggingModel {
    private String email;
    private String password;
}
