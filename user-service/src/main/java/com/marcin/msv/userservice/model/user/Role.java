package com.marcin.msv.userservice.model.user;

public enum Role {
    ADMIN, USER, GUEST
}
