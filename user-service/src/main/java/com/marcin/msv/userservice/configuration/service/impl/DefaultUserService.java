package com.marcin.msv.userservice.configuration.service.impl;

import com.marcin.msv.userservice.configuration.HttpClientConfig;
import com.marcin.msv.userservice.model.user.LoggingModel;
import com.marcin.msv.userservice.model.user.Role;
import com.marcin.msv.userservice.model.user.User;
import com.marcin.msv.userservice.repository.InterestRepository;
import com.marcin.msv.userservice.repository.UserRepository;
import com.marcin.msv.userservice.configuration.service.UserService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
@Import(HttpClientConfig.class)
public class DefaultUserService implements UserService {

    @Autowired
    private HttpClient httpClient;
    private final UserRepository userRepository;
    private final InterestRepository interestRepository;

    @Override
    public User getUserById(Long id) {
        return userRepository.getReferenceById(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public User modifyUserById(Long id, User user) {
        return getUserById(id);
    }

    @Override
    public void changeRoleToUser(Long userId, Role roleToApply) {
        if (userRepository.findById(userId).get().getRole().equals(Role.ADMIN)){
            Optional<User> userToRoleChange = userRepository.findById(userId);
            userToRoleChange.orElseThrow().setRole(roleToApply);
        } else {
            log.warn("Only ADMIN role can change user roles");
        }
    }

    @Override
    public void updateUserData(User oldUserData) {

    }

    @Override
    public User areLoggingDataCorrect(LoggingModel loggingModel) {
        User user = userRepository.findByEmail(loggingModel.getEmail());
        return loggingModel.getPassword().equals(user.getPassword()) ? user : null;
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    public String generateToken(String email) throws IOException, InterruptedException {
        String emailPart = email.split("@")[0];
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:9000/generate-token"))
                .POST(HttpRequest.BodyPublishers.ofString(emailPart))
                .header("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                .build();


        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body();
    }
}
