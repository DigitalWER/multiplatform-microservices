package com.marcin.msv.userservice.repository;

import com.marcin.msv.userservice.model.other.Interest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InterestRepository extends JpaRepository<Interest, Long> {
    Interest findByName(String name);
}
