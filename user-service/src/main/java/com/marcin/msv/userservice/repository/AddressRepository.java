package com.marcin.msv.userservice.repository;

import com.marcin.msv.userservice.model.address.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
