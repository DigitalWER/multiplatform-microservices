package com.marcin.msv.userservice.configuration.service;

import com.marcin.msv.userservice.model.user.LoggingModel;
import com.marcin.msv.userservice.model.user.Role;
import com.marcin.msv.userservice.model.user.User;

import java.io.IOException;
import java.util.List;

public interface UserService {
    User getUserById(Long id);
    List<User> getAllUsers();
    void saveUser(User user);
    User modifyUserById(Long id, User user);
    void changeRoleToUser(Long userId, Role roleToApply);
    void updateUserData(User user);
    String generateToken(String email) throws IOException, InterruptedException;
    User areLoggingDataCorrect(LoggingModel loggingModel);
    void deleteById(Long id);
    void deleteAll();
}
