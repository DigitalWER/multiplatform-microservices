package com.marcin.msv.userservice.configuration;

public interface ApplicationVersion {
    String VERSION_1_0 = "/v1.0";
}
